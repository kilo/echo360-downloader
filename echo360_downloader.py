# This script is a modified version of the original script at ETH Zurich
# found here:
# https://gitlab.ethz.ch/tgeorg/vo-scraper/-/blob/master/vo-scraper.py
# It is licensed under the GNU GPLv3 license.
import argparse
from datetime import datetime
import logging
import shutil
import sys
import os
import getpass
import random
import re
from typing import Optional, TypedDict, List
import requests

REMOTE_URL = "https://git.tardisproject.uk/kilo/echo360-downloader"
REMOTE_ISSUES_URL = REMOTE_URL + "/-/issues"
REMOTE_VERSION_URL = REMOTE_URL + "/-/raw/main/VERSION"
VERSION = "1.0.6"
OUTPUT_IS_TTY = sys.stdout.isatty()


# A quick lookup table for colors to use in the terminal. If the attached output
# is not a console, we will disable the colors.
class Colors:
    DEBUG = "\033[94m" if OUTPUT_IS_TTY else ""
    ERROR = "\033[91m" if OUTPUT_IS_TTY else ""
    WARNING = "\033[93m" if OUTPUT_IS_TTY else ""
    ENDC = "\033[0m" if OUTPUT_IS_TTY else ""


class TargetVideo:
    """A structure for a scraped video that hasn't been downloaded yet."""

    filename: str
    video_src_link: str
    episode_name: str
    title: str
    date: datetime

    def __init__(
        self,
        filename: str,
        video_src_link: str,
        episode_name: str,
        title: str,
        date: datetime,
    ):
        self.filename = filename
        self.video_src_link = video_src_link
        self.episode_name = episode_name
        self.title = title
        self.date = date

    def __repr__(self) -> str:
        return f'TargetVideo(filename="{self.filename}", video_src_link="{self.video_src_link}", episode_name="{self.episode_name}", title="{self.title}", date="{self.date}")'


# A list of hints that randomly get displayed after the script finishes, unless
# the user disables them.
HINT_LIST: List[str] = [
    # --help
    """Want to know more about the script's functionality?
Run `python3 echo360_downloader.py --help` to see all commands that can be used.
For a detailed explanation of some of the commands, check out the README here:
https://git.tardisproject.uk/kilo/echo360-downloader""",
    # --all
    """Want to download all recordings of a lecture at once?
If you use `--all` it will skip the selection screen and download all recordings.
Usage example:

    python3 echo360_downloader.py --all https://echo360.org.uk/section/5158b49c-06c2-4958-a437-0ce3bd977ee6/home""",
    # Bug reporting
    """Found a bug?
Report it directly at https://git.tardisproject.uk/kilo/echo360-downloader/-/issues""",
    # --destination DESTINATION
    """Did you know? By default the echo360_downloader saves the dowloaded recordings in \"Lecture Recordings\" in the current directory.
If you want the recordings saved in a different place you can use the parameter `--destination <your folder>`
For example:

    python3 echo360_downloader.py --destination my_folder https://echo360.org.uk/section/5158b49c-06c2-4958-a437-0ce3bd977ee6/home

saves the recordings inside the folder name \"my_folder\"""",
    # --disable-hints
    """Getting annoyed by this hint message?
You can pass the parameter `--disable-hints` to not show hints after running.""",
    # --file FILE
    """Downloading multiple lectures and tired of having to enter all those links everytime you want to download a recording?
You can paste all your links in a text file and then tell the scraper to read from that file using the parameter `--file <your text file>`
Example:

    python3 echo360_downloader.py --file my_lectures.txt

The scraper will read the links from that file and download them as usual.""",
    # --hide-progress-bar
    """Progress bar breaking your terminal?
Hide it by passing the parameter `--hide-progress-bar`""",
    # --history-file FILE
    """Did you know, that the scraper does not re-download a lecture recording if it detects the recording in its download folder?
This way bandwidth is saved by preventing unecessary re-downloads, especially when using the `--all` parameter to download all existing recordings of a lecture.
However this also mean that if you delete the recording and run the scraper with `--all` again it will re-download the recording.

To fix this you can use the parameter `--history-file <some filename>` which creates a text file with that name and stores a history of all downloaded lectures there.
For example:

    python3 echo360_downloader.py --history-file history.txt <your links>

will create a file called 'history.txt' and save a history of all downloaded recordings there. If you delete a downloaded video the downloader will not redownload it as long as you pass `--history-file <filename> every time you run it.`""",
    # --arguments-file FILE
    """Annoyed by having to type all those parameters like `--all`, `--history-file`, etc. by hand?
You can create a text file called "arguments.txt" and paste all your parameters there. If it's in the same location as the downloader it will automatically read it and apply them.

If you want to use a different name for it, you can pass `--arguments-file <your filename>` to read parameters from `<your filename>` instead.
Ironically this parameter cannot be put into the parameter file.""",
    # --print-source [FILE]
    """Have your own method of downloading videos?
You can use the parameter `--print-source` to print the direct links to the recordings instead of downloading them.
By default the links are printed in your terminal. If you follow up the parameter with a file e.g. `--print-source video_links.txt` a file with that name is created and all the links are saved there. You may need authorization to download the videos.""",
    # --skip-connection-check
    # --skip-update-check
    """In order to ensure functionality, the scraper will check whether your version is up to date and that you have an internet connection.
If you don't like this, you can pass the parameter `--skip-update-check` to prevent the former and `--skip-connection-check` to prevent the latter.""",
    # Tardis
    """Did you know that this script is developed and hosted on Tardis?
Tardis is a part of University of Edinburgh CompSoc, and provides computing services to hobbyists and small organisations, for any non-profit purposes. Our goal is to promote small-scale computing, and provide a safe space for users to learn practical sysadmin and computing skills.

If you're interested, check out our website at https://tardisproject.uk/""",
]


logger = logging.getLogger(__name__)

parser = argparse.ArgumentParser()
parser.add_argument(
    "course_link",
    nargs="*",
    help="A link for each course on Echo360 to download videos from. Should be in the form: https://echo360.org.uk/section/<uuid>/home",
)
parser.add_argument(
    "-a",
    "--all",
    action="store_true",
    help="Download all videos of the specified course(s). Already downloaded videos will be skipped.",
)
parser.add_argument(
    "-d",
    "--destination",
    default="Lecture Recordings" + os.sep,
    help='Directory to save the downloads to. A new subdirectory will be created within it per course. By default this is "Lecture Recordings" in the current directory.',
)
parser.add_argument(
    "--disable-hints",
    action="store_true",
    help="Disable hints that get displayed when the downloader finishes.",
)
parser.add_argument(
    "-f",
    "--file",
    metavar="FILE",
    help="A file containing a list of course links to download videos from. Each line should be in the form: https://echo360.org.uk/section/<uuid>/home",
)
parser.add_argument(
    "--hide-progress-bar",
    action="store_true",
    help="Hide the progress bar when downloading videos.",
)
parser.add_argument(
    "--history-file",
    metavar="FILE",
    help="File to read/write a cache list of downloaded video IDs. The downloader will skip downloading any videos already listed here, in addition to already-existing files. By default this is not used, and downloads will be skipped only if the file exists.",
)
parser.add_argument(
    "--arguments-file",
    metavar="FILE",
    default="arguments.txt",
    help='File to read default command-line arguments from. The contents will be unioned with the command-line parameters. By default this is "arguments.txt" in the current directory.',
)
parser.add_argument(
    "-p",
    "--print-source",
    metavar="FILE",
    nargs="?",
    default=argparse.SUPPRESS,
    help="Prints the source link for each video without downloading. If a file is specified, the links will be written to that file instead. Useful for using with other downloaders.",
)
parser.add_argument(
    "-sc",
    "--skip-connection-check",
    action="store_true",
    help="Skip checking whether there's an internet connection.",
)
parser.add_argument(
    "-su",
    "--skip-update-check",
    action="store_true",
    help="Skip checking for updates for the script.",
)
parser.add_argument(
    "-v",
    "--verbose",
    action="store_true",
    help="Prints additional debugging information.",
)
parser.add_argument(
    "--version",
    action="store_true",
    help="Prints the current version of the script and exit.",
)


def main(args: argparse.Namespace) -> int:
    """Central logic of the script.

    Parameters
    ----------
    args : argparse.Namespace
        Arguments passed to the script.

    Returns
    -------
    int
        Exit code
    """
    logger.debug("This log line is only visible if --verbose flag is set.")

    if args.version:
        logging.info(VERSION)
        return 0

    links = args.course_link
    if args.file:
        with open(args.file, "r") as f:
            # Ignore empty lines and lines starting with #
            links += filter(
                lambda line: line.strip() and line.strip().startswith("#"),
                f.read().split(),
            )

    if not links:
        logger.error("No course links specified. See --help for more information.")
        return 1

    # Check that we have an internet connection to download with
    if not args.skip_connection_check:
        try:
            requests.get("https://echo360.org.uk")
        except requests.exceptions.ConnectionError:
            logger.error("No internet connection.")
            return 1
    else:
        logger.info("Skipping internet connection check.")

    # If there are updates, we will warn but not fail
    if not args.skip_update_check:
        try:
            remote_version = requests.get(REMOTE_VERSION_URL).text.strip()
            if remote_version != VERSION:
                logger.warning(
                    f"New version available: {remote_version}. You are using {VERSION}."
                )
                logger.warning(
                    f"If you encounter issues, we recommend re-downloading the script from {REMOTE_URL}."
                )
        except requests.exceptions.ConnectionError:
            logger.warning("Could not check for updates.")
    else:
        logger.info("Skipping update check.")

    video_target_collection: List[TargetVideo] = []

    # Reuse the same session for all requests (to keep auth cookies if any).
    session = requests.Session()
    for link in links:
        # For each provided link, we'll scrape the videos available (optionally
        # asking for auth), then interactively ask the user which ones they want
        # to download.
        logger.info("Currently selected: " + link)
        if "echo360.org.uk/section" not in link:
            logger.warning(
                "Looks like the link doesn't go to 'echo360.org.uk' and therefore has been skipped. Please make sure that it is correct: "
                + link
            )

            if "youtube" in link or "youtu.be" in link:
                logger.warning(
                    "If the video is on YouTube, you can use the tools youtube-dl or yt-dlp."
                )

            continue

        available_videos = scrape_videos(link, session)
        logging.info(f"Found {len(available_videos)} videos.")

        if len(available_videos) == 0:
            continue

        pretty_print_videos(available_videos)

        # Ask the user for indices of videos to download
        selection: List[int] = []
        if args.all:
            selection = list(range(len(available_videos)))
        else:
            selection = interactive_video_selection(available_videos)

        # Confirm selection
        logger.info("Selected videos: ")
        pretty_print_videos(available_videos, filters=selection)

        # Add selected videos to the collection
        for i in selection:
            video_target_collection.append(available_videos[i])

    # Print an overview before starting to download if debugging
    logger.debug(f"Videos to download: {video_target_collection}")

    # Download selected episodes
    for video in video_target_collection:
        # If only printing to file, do so and skip downloading
        if "print_source" in args:
            if args.print_source:
                logger.debug(
                    f"Printing {video.video_src_link} to file: {args.print_source}"
                )
                with open(args.print_source, "a") as f:
                    f.write(video.video_src_link + "\n")
            else:
                logger.info(video.video_src_link)
            continue

        # If a history file is specified, check if we've already listed it
        if args.history_file:
            try:
                with open(args.history_file, "r") as f:
                    if video.video_src_link in f.read():
                        logger.info(
                            f"Skipping {video.episode_name}: Already in history.txt."
                        )
                        continue
            except FileNotFoundError:
                logger.warning("No history file found at " + args.history_file)
                logger.warning("Creating a new one.")

            # Add it to the history file
            with open(args.history_file, "a") as f:
                f.write(video.video_src_link + "\n")

        # Before download, prepend the destination directory
        directory_prefix = args.destination
        logger.debug("Destination dir: " + directory_prefix)
        if not directory_prefix.endswith(os.sep):
            # Add trailing (back)slash as the user might have forgotten it
            directory_prefix += os.sep
            logger.debug("Added missing slash: " + directory_prefix)

        download(
            directory_prefix + video.filename,
            video.video_src_link,
            video.episode_name,
            args.hide_progress_bar,
            session,
        )

    # Display hints if applicable
    if not args.disable_hints and HINT_LIST and video_target_collection:
        logger.info("")
        logger.info("-" * shutil.get_terminal_size().columns)
        logger.info("Hint:")
        logger.info(random.choice(HINT_LIST))
        logger.info("-" * shutil.get_terminal_size().columns)

    return 0


def scrape_videos(link: str, session: requests.Session) -> List[TargetVideo]:
    syllabus_link = create_syllabus_link(link)
    if not syllabus_link:
        logger.error("URL format is not valid.")
        return []

    # Try to get it on the first try, if it fails, try logging in
    r = session.get(syllabus_link)
    if "login.echo360.org.uk" in r.url:
        username = ease_login(session)
        if not username:
            logger.error("Could not log into EASE (invalid credentials?).")
            return []

        # Extract appId from the redirect URL, which is alphanumeric with
        # dashes.
        app_id = re.search(r"appId=([A-Za-z0-9-]+)", r.url)
        if not app_id:
            logger.error("Could not log into Echo360 (appId not found).")
            return []

        success = echo360_login(session, username, app_id.group(1))
        if not success:
            logger.error("Could not log into Echo360 (SAML error?).")
            return []
        r = session.get(syllabus_link)

    # Attempt to parse response as JSON, which might fail if the second attempt
    # to get the syllabus page failed.
    try:
        response = r.json()
    except ValueError:
        logger.error("Could not parse JSON response.")
        return []

    # Sanity-check: Check if the JSON response says it's OK
    if "status" not in response or response["status"] != "ok":
        logger.error(f"Could not get lecture syllabus: {response['status']}")
        return []

    # Loop through each lecture recording
    targets: List[TargetVideo] = []
    for i, lecture_session in enumerate(response["data"]):
        # Make sure the fields we need are present
        if (
            lecture_session["type"] != "SyllabusLessonType"
            or "lesson" not in lecture_session
            or "lesson" not in lecture_session["lesson"]
            or "medias" not in lecture_session["lesson"]
        ):
            logger.debug(
                f"Skipping {lecture_session['lesson']['name']}: Invalid format."
            )
            continue

        # Just a bunch of sanity checking
        if not lecture_session["lesson"].get("isPast", False):
            logger.debug(
                f"Skipping {lecture_session['lesson']['lesson']['name']}: Not past."
            )

        if not lecture_session["lesson"].get("hasContent", False):
            logger.debug(
                f"Skipping {lecture_session['lesson']['lesson']['name']}: No content."
            )

        if not lecture_session["lesson"].get("hasVideo", False):
            logger.debug(
                f"Skipping {lecture_session['lesson']['lesson']['name']}: No video."
            )

        # Make sure there's at least one media because we'd make unnecessary
        # requests otherwise
        medias = lecture_session["lesson"]["medias"]
        if len(medias) == 0:
            logger.debug(
                f"Skipping {lecture_session['lesson']['lesson']['name']}: No media."
            )

        # Get the ID and send request to get the video link for the best media
        lecture_id = lecture_session["lesson"]["lesson"]["id"]

        # Print some dots to indicate progress. These will break if the scraping
        # logs something, but that's fine since it'll only happen if something
        # is wrong.
        sys.stdout.write(f"\rGetting metadata and video links" + "." * i)
        sys.stdout.flush()
        targets += scrape_videos_for_lecture(lecture_id, session)

    return targets


def scrape_videos_for_lecture(
    lecture_id: str,
    session: requests.Session,
) -> List[TargetVideo]:
    r = session.get(f"https://echo360.org.uk/lesson/{lecture_id}/media")

    # Attempt to parse response as JSON
    try:
        response = r.json()
    except ValueError:
        logger.error(f"Could not parse JSON response for lecture {lecture_id}.")
        return []

    # Sanity-check: Check if the JSON response says it's OK
    if "status" not in response or response["status"] != "ok":
        logger.error(f"Could not get videos for {lecture_id}: {response['status']}")
        return []

    # Make sure video-related fields are present
    if (
        len(response["data"]) == 0
        or "video" not in response["data"][0]
        or "media" not in response["data"][0]["video"]
        or response["data"][0]["video"]["media"].get("status", "") != "Processed"
        or "media" not in response["data"][0]["video"]["media"]
        or "current" not in response["data"][0]["video"]["media"]["media"]
    ):
        logger.debug(f"Skipping {lecture_id}: Lecture doesn't have videos.")
        return []

    # Make sure metadata is present
    if (
        "userSection" not in response["data"][0]
        or "sectionNumber" not in response["data"][0]["userSection"]
    ):
        logger.debug(f"Skipping {lecture_id}: Lecture has no metadata.")
        return []

    course_title = response["data"][0]["userSection"]["sectionNumber"]

    lecture_title = response["data"][0]["video"]["media"].get("name", "Lecture")

    # fromisoformat() with the Z timezone requires Python 3.11+, so just replace
    # the Z with +00:00
    video_date = datetime.fromisoformat(
        response["data"][0]["video"]["media"]["createdAt"].replace("Z", "+00:00")
    )

    # Each media object is structured like:
    # {
    # "s3Url": "https://content.echo360.org.uk/unique-url/1/name.extension",
    # "width": 480,
    # "height": 270,
    # "size": 59407080
    # }
    MediaJsonType = TypedDict(
        "MediaJsonType", {"s3Url": str, "width": int, "height": int, "size": int}
    )

    # Get both camera tracks if possible.
    primary_media: List[MediaJsonType] = response["data"][0]["video"]["media"][
        "media"]["current"].get("primaryFiles", [])
    secondary_media: List[MediaJsonType] =  response["data"][0]["video"]["media"][
        "media"]["current"].get("secondaryFiles", [])

    # Select largest video (per track) by resolution, and resolve ties with larger filesize
    for media in [primary_media, secondary_media]:
        media.sort(key=lambda media: (media["height"], media["size"]), reverse=True)

    video_id = response["data"][0]["video"]["media"]["media"]["current"][
        "mediaId"].split("-")[0]
    
    videos_to_return = []
    for track_label, track_media in [("primary", primary_media), ("secondary", secondary_media)]:
        if track_media:
            videos_to_return.append(
                TargetVideo(
                    filename=remove_illegal_characters(course_title)
                    + os.sep
                    + remove_illegal_characters(
                        video_date.strftime("%Y-%m-%d") + "-" + video_id +
                         ("-" + track_label if primary_media and secondary_media else "") + ".mp4"
                    ),
                    video_src_link=track_media[0]["s3Url"],
                    title=lecture_title + (" [" + track_label + "]" if primary_media and secondary_media else ""),
                    date=video_date,
                    episode_name=video_date.strftime("%Y-%m-%d") + " " + lecture_title +
                        (" [" + track_label + "]" if primary_media and secondary_media else ""),
                )
            )

    return videos_to_return


def create_syllabus_link(link: str) -> Optional[str]:
    """Accept any link to a course on Echo360 and return the link to the JSON
    syllabus page.

    Parameters
    ----------
    link : str
        Any URL matching Echo360.org.uk/section/<uuid>, with optionally some
        other stuff after it.

    Returns
    -------
    Optional[str]
        The link to the JSON syllabus page, or None if the link is not valid.
    """
    pat = re.compile(r"https?\:\/\/echo360\.org\.uk\/section\/([A-Za-z0-9-]+)(\/.*)?")
    match = re.match(pat, link)
    if match:
        return f"https://echo360.org.uk/section/{match.group(1)}/syllabus"

    return None


def pretty_print_videos(
    videos: List[TargetVideo], filters: Optional[List[int]] = None
) -> None:
    """Prints a list of videos in a nice format.

    Parameters
    ----------
    videos : List[TargetVideo]
        List of videos to print.
    filters : Optional[List[int]], optional
        List of indices of videos to show. If None, all videos will be shown,
        and if empty list, no videos will be shown.
    """
    if len(videos) == 0:
        return

    nr_length = len(" Nr.")
    max_date_len = max([len(video.date.strftime("%Y-%m-%d")) for video in videos])
    max_title_len = max([len(video.title) for video in videos])

    # Print header first
    logger.info(
        " Nr."
        + " | "
        + "Date".ljust(max_date_len)
        + " | "
        + "Title".ljust(max_title_len)
    )

    for i, video in enumerate(videos):
        # Skip if filters are specified and this video is not in them
        if filters is not None and i not in filters:
            continue

        # Show indices if we're showing all videos, otherwise only show a star
        # to indicate that this video is selected
        nr = Colors.WARNING + "  * " + Colors.ENDC if filters else f"{i:3d}"
        logger.info(
            nr.ljust(nr_length)
            + " | "
            + video.date.strftime("%Y-%m-%d").ljust(max_date_len)
            + " | "
            + video.title.ljust(max_title_len)
        )


def interactive_video_selection(
    available_videos: List[TargetVideo],
) -> List[int]:
    """Interactively asks the user which videos to download, returns the indices
    for selected videos.

    Parameters
    ----------
    available_videos : List[TargetVideo]
        Videos to select from.

    Returns
    -------
    List[int]
        Indices of selected videos.
    """
    while True:
        user_input = input(
            "Enter numbers of the above lectures you want to download separated by "
            "space (e.g. 0 5 12 14)\nYou can also write ranges as X-Y (e.g. 0-5 8)."
            "\nJust press enter if you don't want to download anything.\n"
        ).split()

        # Parse the user input
        selection: List[int] = []
        for item in user_input:
            # Check if it's a range
            if "-" in item:
                try:
                    start, end = item.split("-")
                    selection += list(range(int(start), int(end) + 1))
                except ValueError:
                    logger.warning(f"Invalid range: {item}")
                    break
            else:
                try:
                    selection.append(int(item))
                except ValueError:
                    logger.warning(f"Invalid number: {item}")
                    break

        # No problem with parsing, so stop asking
        else:
            break

    # Make elements unique
    selection = list(set(selection))

    # Sort them, to download in order and not randomly
    selection = sorted(selection)

    return selection


def remove_illegal_characters(file_name: str) -> str:
    """Remove characters that the file system doesn't like, including slashes.
    This function should be used for individual components of a path, not on the
    entire path.

    Parameters
    ----------
    file_name : str
        File name to remove illegal characters from.

    Returns
    -------
    str
        Sanitized file name.
    """
    illegal_chars = '?<>:*|"^/\\'
    for c in illegal_chars:
        file_name = file_name.replace(c, "")
    return file_name


def ease_login(session: requests.Session) -> Optional[str]:
    """Perform EASE login and setup the session with the logged-in cookies, so
    that further requests using SAML will work.

    Parameters
    ----------
    session : requests.Session

    Returns
    -------
    Optional[str]
        EASE username if login was successful, None otherwise.
    """
    logging.info(
        "This course requires authentication. Please provide your EASE credentials."
    )
    username = input("Username: ")
    password = getpass.getpass("Password: ")

    # Get once to set the cookies
    logger.info("Retrieving EASE cookies...")
    session.get("https://www.ease.ed.ac.uk/")

    # Login to CoSign
    logger.info("Logging into EASE CoSign...")
    r = session.post(
        "https://www.ease.ed.ac.uk/cosign.cgi",
        data={"login": username, "password": password},
    )

    # Check if we have a logout button, which means we're logged in
    if "/logout/logout.cgi" not in r.text:
        return None

    return username


def echo360_login(session: requests.Session, username: str, app_id: str) -> bool:
    """Perform the Echo360 login procedure, which uses SAML and needs the
    session to already have the EASE identity provider cookies.

    Parameters
    ----------
    session : requests.Session
    username : str
        EASE username, to which we append "@ed.ac.uk" to get the SAML username.
    app_id : str
        SAML appId, which is in the redirect URL on the Echo360 login page.

    Returns
    -------
    bool
        Whether the login was successful.
    """
    logging.debug(
        f"Logging into Echo360 using SAML username/appId: {username}, {app_id}"
    )

    # Retrieve appId
    logger.info("Retrieving Echo360 cookies...")
    r = session.post(
        "https://login.echo360.org.uk/login/institutions",
        data={
            "email": username + "@ed.ac.uk",
            "appId": app_id,
            "role": "",
            "requestedResource": "",
        },
    )

    # Get the value of the SAMLResponse input field
    logger.info("Retrieving Edinburgh IDP SAMLResponse...")
    saml_response = re.search(
        r"<input type=\"hidden\" name=\"SAMLResponse\" value=\"(.*)\"/>", r.text
    )
    if not saml_response:
        logging.error("Could not find SAMLResponse input field.")
        return False

    saml_response = saml_response.group(1)
    logging.debug(f"SAMLResponse: {saml_response}")

    # Get the value of the RelayState input field
    relay_state = re.search(
        r"<input type=\"hidden\" name=\"RelayState\" value=\"(.*)\"/>", r.text
    )
    if not relay_state:
        logging.error("Could not find RelayState input field.")
        return False

    relay_state = relay_state.group(1)
    logging.debug(f"RelayState: {relay_state}")

    # Login to Echo360, which uses PingIdentity under the hood
    logger.info("Sending Echo360 the SAMLResponse...")
    r = session.post(
        "https://sso.connect.pingidentity.com/sso/sp/ACS.saml2",
        data={"SAMLResponse": saml_response, "RelayState": relay_state},
    )

    if "/sso/pages/error" in r.text:
        logging.error("SAML error.")
        return False

    # Check if we have a Resume button to press on
    logger.info("Automating button press to login...")
    if "Resume" not in r.text:
        logging.error("Could not find Resume button.")
        return False

    # Get the form action URL and various other fields
    action_url = re.search(r"<form method=\"post\" action=\"(.*)\">", r.text)
    token_id = re.search(
        r"<input type=\"hidden\" name=\"tokenid\" value=\"(.*)\"/>", r.text
    )
    agent_id = re.search(
        r"<input type=\"hidden\" name=\"agentid\" value=\"(.*)\"/>", r.text
    )
    if not action_url or not token_id or not agent_id:
        logging.error("Could not find form action URL.")
        return False

    action_url = action_url.group(1)
    token_id = token_id.group(1)
    agent_id = agent_id.group(1)

    # Press the Resume button
    r = session.post(
        action_url,
        data={
            "tokenid": token_id,
            "agentid": agent_id,
        },
    )

    return True


def download(
    file_name: str,
    video_src_link: str,
    episode_name: str,
    hide_progress_bar: bool,
    session: requests.Session,
) -> None:
    # Create directory for video if it does not already exist
    directory = os.path.dirname(os.path.abspath(file_name))
    if not os.path.isdir(directory):
        os.makedirs(directory)
        logger.debug("This folder was generated: " + directory)
    else:
        logger.debug("This folder already exists: " + directory)

    # Check if file already exists
    if os.path.isfile(file_name):
        logger.info("Download skipped - file already exists: " + episode_name)
        return

    # cf.: https://stackoverflow.com/questions/15644964/python-progress-bar-and-downloads
    with open(file_name + ".part", "wb") as f:
        response = session.get(video_src_link, stream=True)
        total_length = response.headers.get("content-length")

        logger.info(
            f"Downloading {episode_name} ({int(total_length or 0) / 1024 / 1024:.2f} MiB)"
        )

        if total_length is None or hide_progress_bar:
            # We received no content length header...
            # ... or user wanted to hide the progress bar
            f.write(response.content)
        else:
            # Download file and show progress bar
            total_length = int(total_length)

            try:
                # Module with better progressbar
                from tqdm import tqdm

                # Setup progressbar
                pbar = tqdm(
                    unit="B", unit_scale=True, unit_divisor=1024, total=total_length
                )
                pbar.clear()

                # Download to file and update progressbar
                for data in response.iter_content(chunk_size=4096):
                    pbar.update(len(data))
                    f.write(data)
                # Close it
                pbar.close()

            # If tqdm is not installed, fallback to self-made version
            except ModuleNotFoundError:
                logger.debug(
                    "Optionally dependency tqdm not installed, falling back to built-in progressbar"
                )
                dl = 0
                for data in response.iter_content(chunk_size=4096):
                    dl += len(data)
                    f.write(data)
                    progressbar_width = shutil.get_terminal_size().columns - 2
                    done = int(progressbar_width * dl / total_length)
                    sys.stdout.write(
                        f"\r[{'=' * done}{' ' * (progressbar_width - done)}]"
                    )
                    sys.stdout.flush()
    print()

    # Remove `.part` suffix from file name
    os.rename(file_name + ".part", file_name)
    logger.info("Downloaded file: " + episode_name)


if __name__ == "__main__":
    args = parser.parse_args()

    # Before handing the main function the CLI arguments, check if an argument
    # file was specified and if so, read it and union it with the CLI arguments,
    # with the CLI arguments taking precedence.
    if args.arguments_file != "":
        try:
            with open(args.arguments_file, "r") as f:
                args = parser.parse_args(f.read().split() + sys.argv[1:])
        except FileNotFoundError:
            pass

    # Set up so the default log level is everything above INFO. This is so that
    # we can use always use logger.info() instead of print() for normal output,
    # making the distinction less confusing for the developers.
    logging.basicConfig(
        format="%(levelname)s%(message)s",
        level=logging.DEBUG if args.verbose else logging.INFO,
    )

    # Color the log level prefixes unless the output is INFO or is piped
    logging.addLevelName(logging.INFO, f"")
    logging.addLevelName(logging.ERROR, f"{Colors.ERROR}ERROR{Colors.ENDC} ")
    logging.addLevelName(logging.WARNING, f"{Colors.WARNING}WARN{Colors.ENDC} ")
    logging.addLevelName(logging.DEBUG, f"{Colors.DEBUG}DEBUG{Colors.ENDC} ")

    logger.debug(f"Parsed Arguments: {args}")

    # Main logic
    sys.exit(main(args))
